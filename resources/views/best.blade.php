<ul>
    <div class="list-group">
        <h2 class="text-center">Лучшие скидки</h2>

        @foreach($constant_data['best_items'] as $best_item)
            <a href="{{ $best_item['link'] }}" class="list-group-item list-group-item-action">
                <h5 class="list-group-item-heading">{{ $best_item['title'] }}</h5>
                <p class="list-group-item-text">{{ $best_item['description'] }}</p>
            </a>
        @endforeach

    </div>
</ul>