@extends('layouts.app')

@section('content')

    <main role="main" class="container content">
        <div class="contact-us__wrap">
            <div class="col-12 col-md-8 offset-md-2 contact-us__form-wrap">
                <h2>Обратная связь</h2>
                <p>Мы хотели бы услышать ваши отзывы на сайте или если у вас возник конкретный вопрос для нас, мы будем рады Вам ответить. Заполните форму ниже, и мы свяжемся с вами как можно скорее.</p>
                <form id="contact-us__form">
                    @csrf
                    <div class="form-group">
                        <input class="form-control input-lg" id="contact-us__firstname" placeholder="Имя*" data-required="" data-required-id="field_firstname" type="text">
                    </div>

                    <div class="form-group">
                        <input class="form-control input-lg" id="contact-us__lastname" placeholder="Фамилия*" data-required="" data-required-id="field_lastname" type="text">
                    </div>

                    <div class="form-group">
                        <input class="form-control input-lg" id="contact-us__email" placeholder="E-Mail*" data-required="" data-required-id="field_email" type="email">
                    </div>

                    <div class="form-group contact-us__enquiryType-wrap">
                        <select id="contact-us__enquiryType" class="form-control input-lg" title="Тип запроса*" data-width="100%" data-required="" data-required-id="field_enquiry">
                            <option class="bs-title-option" value="-1" disabled selected>-- Пожалуйста выберите --</option>
                            <option value="Business Opportunity">Бизнес-возможности</option>
                            <option value="Feedback">Обратная связь</option>
                            <option value="Media Enquiry">Медиа-запрос</option>
                            <option value="Other">Другое</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control input-lg" rows="6" id="contact-us__description" placeholder="Сообщение*" data-required="" data-required-id="field_description"></textarea>
                    </div>

                    <p id="contact-us__error" class="alert alert-danger" role="alert"></p>

                    <button id="contact-us__submit" type="submit" class="btn btn-primary btn-lg">
                        <b><span id="contact-us__loading" class="ico-spinner ico-spin"></span> Отправить</b>
                    </button>

                </form>
            </div>
            <div id="contact-us__submitted" class="col-12 col-md-8 offset-md-2">
                <div class="row">
                    <div class="col-3 contact-us__submitted-ico">
                        <span class="ico-check-circle"></span>
                    </div>
                    <div class="col-9">
                        <h2 class="contact-us__submitted-title mt-5">Thanks</h2>
                        <p class="mb-5">
                            Спасибо... ваш запрос отправлен. Мы свяжемся с вами как можно скорее.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

@section('footer-scripts')


@endsection
