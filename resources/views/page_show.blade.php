@extends('layouts.app')

@section('content')

    <main role="main" class="container content">

        @if(!empty($page_data))
            <div class="container mt-3">
                <div class="col-12 text-center">
                    <h1>{{ $page_data['title'] }}</h1>
                </div>

                <div class="text-center">
                    <img src="{{ asset($page_data['image']) }}" title="{{ $page_data['title'] }}">
                </div>
            </div>

            <div class="container">
                {!! $page_data['description'] !!}
            </div>

            <div class="container text-center ">
                <a href="{{ $page_data['link'] }}" class="btn btn-success btn-lg">Воспользоваться скидкой!</a>
            </div>

            @guest
            @else
                <hr>
                <div class="container">
                    <div class="row">
                        <div class="col-6 text-center">
                            <form method="post" action="{{ $page_data['id'] }}">
                                @csrf
                                <input name="_method" type="hidden" value="delete" />
                                <input type="submit" class="btn btn-danger btn-lg" value="Удалить">
                            </form>
                        </div>
                        <div class="col-6 text-center">
                            <a href="{{ $page_data['id'] }}/edit" class="btn btn-info btn-lg">Редактировать</a>
                        </div>
                    </div>
                </div>
            @endguest

        @else

            <div class="container mt-3">
                <div class="col-12 text-center">
                    <h1>Oooops! Page not found :(</h1>
                </div>
            </div>

        @endif

    </main>

@endsection
