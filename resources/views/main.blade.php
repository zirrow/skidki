@extends('layouts.app')

@section('content')

    <main role="main" class="container content">

        @if(isset($update_status))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Запись обновлена успешно!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if(isset($destroy_status))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Запись удалена успешна!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="category-items__wrap row">

            @foreach($pages as $page)
                <div class="category-items_item-content col-12 col-sm-6 col-md-4 p-1 mb-4">

                    <div class="category-items__item p-1 clearfix" data-href="page/{{ $page['id'] }}">
                        <div class="category-items__item-image">
                            <img src="{{ $page['image'] }}" alt="{{ $page['title'] }}">
                        </div>
                        <div class="category-items__item-title">
                            {{ $page['title'] }}
                        </div>
                        <div class="category-items__item-description">
                            {{ $page['description'] }}
                        </div>
                        @guest

                        @else
                            <div class="category-items__item-footer">
                                <form method="post" action="page/{{ $page['id'] }}">
                                    @csrf
                                    <input name="_method" type="hidden" value="delete" />
                                    <input type="submit" class="btn btn-danger btn-sm" value="Удалить">
                                </form>

                                <a href="page/{{ $page['id'] }}/edit" class="btn btn-info btn-sm">Редактировать</a>
                            </div>
                        @endguest

                    </div>
                </div>

            @endforeach

        </div>

        <div class="category-items__more show-more-block">
            <a href="#" class="show-more-btn">Еще! <span class="ico_down"></span></a>
        </div>
    </main>

@endsection

@section('footer-scripts')

    <script>
        //разрешаем выводить данные
        var block = true;
        //номер страницы для вывода
        var page = 0;
        //скроллинг

        $('.show-more-btn').on('click', function (e) {
            e.preventDefault();

            block = false;
            page++;

            $.ajax({
                type: "POST",
                url: "{{ route('main-filter') }}",
                data: {'page' : page},
                dataType: 'json',

                success: function (array) {



                    if (array == ''){

                        $('.show-more-block').hide();

                        block = false;
                    } else {

                        block = true;

                        var html = '';

                        $.each(array, function(index, value){

                            // console.log(value['title']);

                            html +=  '<div class="category-items_item-content col-12 col-sm-6 col-md-4 p-1 mb-4">';
                            html +=  '<div class="category-items__item p-1 clearfix" data-href="page/'+ value.id +'">';
                            html +=  '<div class="category-items__item-image">';
                            html +=  '<img src="'+ value.image +'" alt="'+ value.title +'">';
                            html +=  '</div>';
                            html +=  '<div class="category-items__item-title">'+ value.title +'</div>';
                            html +=  '<div class="category-items__item-description">'+ value.description +'</div>';

                            @guest

                            @else
                            html +=  '<div class="category-items__item-footer">';
                            html +=  '<form method="post" action="page/'+ value.id +'">';
                            html +=  '<input name="_method" type="hidden" value="delete" />';
                            html +=  '<input type="submit" class="btn btn-danger btn-sm" value="Удалить">';
                            html +=  ' </form>';

                            html +=  '<a href="page/'+ value.id +'/edit" class="btn btn-info btn-sm">Редактировать</a>';
                            html +=  '</div>';
                            @endguest

                            html +=  '</div>';
                            html +=  '</div>';
                        });

                        $('.category-items__wrap').append(html);
                    }
                }
            });

        });

    </script>

@endsection
