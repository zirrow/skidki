<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($page_title)) {{$page_title}} |@endif {{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="@if(isset($page_description)) {{$page_description}} @endif">
    <meta name="keywords" content="">

    <link rel="icon" href="{{ asset('images/favicon.ico') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div class="content_wrap">

    <header class="container-fluid  fixed-top">
        <nav class="navbar navbar-expand-lg navbar-light">

            <div class="navbar-brand">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('images/logo.jpg') }}" alt="{{ config('app.name', 'discount') }}" class="d-inline align-top">
                </a>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav__items" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse collapse" id="nav__items">
                <ul class="navbar-nav ml-auto mb-2" id="nav__buttons">

                    @guest
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                        {{--</li>--}}

                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        {{--</li>--}}
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="/page">
                                    Добавить
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @include('best')
            </div>
            <div class="col-md-8">
                @yield('content')
            </div>
        </div>
    </div>

    <footer>

        <div class="footer-content container">

            <div class="container">
                <div class="text-center">Всеукраинский дисконтный портал</div>
                <h6 class="text-muted" style="font-size: 13px;">Все дисконты: скидки, распродажи, акции, бонусы, подарки, сертификаты, купоны и флаера на одном сайте. DiscountUA — любая покупка со скидкой! Скидки на обувь, распродажа модной одежды, сезонные распродажи и новогодние скидки в магазинах Киева и Украины.</h6>
            </div>

            <div class="clearfix">

                <ul class="footer-menu list-inline float-left">
                    <li class="list-inline-item">
                        <a class="nav-link" href="{{ route('contact') }}">Напишите нам</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>

                    {{--<li class="list-inline-item">--}}
                        {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                    {{--</li>--}}

                    <li class="list-inline-item">
                        <a class="nav-link" href="mail:info@discount.co.ua">info@discount.co.ua</a>
                    </li>
                </ul>

                <div class="float-right">
                    <div class="back-to-top">
                        <a href="#" id="to-top">Вверх ></a>
                    </div>
                </div>

            </div>
            <div class="copyright text-left">
                © 2019 - {{ config('app.name') }}
            </div>
        </div>

    </footer>
</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('footer-scripts')

</body>
</html>
