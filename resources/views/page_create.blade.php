@extends('layouts.app')

@section('content')

    <main role="main" class="container content">

        @if(isset($add_status))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Запись добавлена успешно! Ссылка - {{ URL("/page/{$add_status}") }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <form method="POST" @if(isset($page_data)) action="/page/{{ $page_data['id'] }}" @else action="" @endIf enctype="multipart/form-data">

            @if(isset($page_data))
                <input name="_method" type="hidden" value="PUT" />
            @endIf

            @csrf

            <div class="form-group row">
                <label for="form-title" class="col-sm-2 col-form-label">Заголовок</label>
                <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" id="form-title" @if(isset($page_data['title'])) value="{{ $page_data['title'] }}" @endIf required>
                </div>
            </div>

            <div class="form-group row">
                <label for="form-desc" class="col-sm-2 col-form-label">Описание</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" id="form-desc" rows="10" required>
                        @if(isset($page_data['description'])) {{ $page_data['description'] }} @endIf
                    </textarea>
                </div>
            </div>

            <div class="form-group row">
                <label for="form-file" class="col-sm-2 col-form-label">Картинка</label>
                <div class="col-sm-10">
                    @if(isset($page_data['image']))
                        <img src="{{ asset($page_data['image']) }}" style="height: 100px; width: 100px">
                    @else
                        <input type="file" name="image" class="form-control-file" id="form-file">
                    @endIf

                </div>
            </div>

            <div class="form-group row">
                <label for="form-link" class="col-sm-2 col-form-label">Ссылка</label>
                <div class="col-sm-10">
                    <input type="url" name="link" class="form-control" id="form-link" @if(isset($page_data['link'])) value="{{ $page_data['link'] }}" @endIf required>
                </div>
            </div>

            <div class="form-group row">
                <label for="form-date" class="col-sm-2 col-form-label">Дата окончания переадресации</label>
                <div class="col-sm-10">
                    <input type="text" name="stop_date" class="form-control" id="form-date" @if(isset($page_data['stop_date'])) value="{{ $page_data['stop_date'] }}" @endIf>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success btn-lg form-control" >
            </div>

        </form>

    </main>


@endsection

@section('footer-scripts')

    <script>
        $('#form-date').datetimepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd HH:MM:SS'
        });

        $(document).ready(function () {
            $("#form-desc").editor();
        });
    </script>

@endsection
