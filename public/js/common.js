(function($) {
    // common sript
    var common = {
        options: {
            elms: {
                toTop: '#to-top',
                categoryItem: '.category-items__item'
            }
        },
        init: function() {
            var _t = this,
                _o = _t.options;
            _t.bind();
        },
        bind: function() {
            var _t = this,
                _o = _t.options;

            $(_o.elms.toTop).on('click', function(e) {
                e.preventDefault();
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            });

            $('.category-items__wrap').on('click', _o.elms.categoryItem, function(e){
                e.stopPropagation();
                location = $(this)[0].dataset['href'];
            });
        }
    };
    // contact_us script
    var contact_us = {
        options: {
            elms: {
                wrap: '.contact-us__wrap',
                form_wrap: '.contact-us__form-wrap',
                form: '#contact-us__form',
                field_firstname: '#contact-us__firstname',
                field_lastname: '#contact-us__lastname',
                field_email: '#contact-us__email',
                field_enquiry_wrap: '.contact-us__enquiryType-wrap',
                field_enquiry: '#contact-us__enquiryType',
                field_description: '#contact-us__description',
                submit: '#contact-us__submit',
                loading: '#contact-us__loading',
                error: '#contact-us__error',
                submitted: '#contact-us__submitted'
            },
            form: {
                field_firstname: '',
                field_lastname: '',
                field_email: '',
                field_enquiry: '',
                field_description: '',
            },
            errors: [],
            error_messages: {
                error: 'Sorry, an error has occurred submitting your feedback',
                requiredFields: 'Please complete required fields',
                captchaFailed: 'Please tick the box to confirm you are not a robot'
            }
        },
        init: function() {
            var _t = this,
                _o = _t.options;
            _t.bind();
        },
        bind: function() {
            var _t = this,
                _o = _t.options;
            $(_o.elms.field_firstname).on('keyup', function() {
                if ($(this).val() !== '') {
                    _o.form.field_firstname = $(this).val();
                    $(this).removeClass('is-invalid');
                    _t.resetErrors();
                    _t.isShowEnquiry();
                }
            });
            $(_o.elms.field_lastname).on('keyup', function() {
                if ($(this).val() !== '') {
                    _o.form.field_lastname = $(this).val();
                    $(this).removeClass('is-invalid');
                    _t.resetErrors();
                    _t.isShowEnquiry();
                }
            });
            $(_o.elms.field_email).on('keyup', function() {
                if ($(this).val() !== '') {
                    _o.form.field_email = $(this).val();
                    $(this).removeClass('is-invalid');
                    _t.resetErrors();
                    _t.isShowEnquiry();
                }
            });
            $(_o.elms.field_enquiry).on('change', function() {
                if ($(this).val() !== '') {
                    _o.form.field_enquiry = $(this).val();
                    $(this).removeClass('is-invalid');
                    _t.resetErrors();
                }
            });
            $(_o.elms.field_description).on('keyup', function() {
                if ($(this).val() !== '') {
                    _o.form.field_description = $(this).val();
                    $(this).removeClass('is-invalid');
                    _t.resetErrors();
                }
            });
            $(_o.elms.wrap).on('submit', _o.elms.form, function(e) {
                e.preventDefault();
                if (_t.formValidation()) {
                    _t.submit();
                } else {
                    _t.showErrors();
                }
            });
        },
        isShowEnquiry: function() {
            var _t = this,
                _o = _t.options;
            if (_o.form.field_firstname.trim() !== '' && _o.form.field_lastname.trim() !== '' && _o.form.field_email.trim() !== '') {
                $(_o.elms.field_enquiry_wrap).show();
            }
        },
        submit: function() {
            var _t = this,
                _o = _t.options;
            $(_o.elms.loading).css('display', 'inline-block');
            $(_o.elms.submit).prop('disabled', true);
            $(_o.elms.error).html('').hide();
            $.ajax({
                url: '/contact-us',
                data: {
                    firstName: _o.form.field_firstname.trim(),
                    lastName: _o.form.field_lastname,
                    email: _o.form.field_email.trim(),
                    enquiryType: _o.form.field_enquiry.trim(),
                    description: _o.form.field_description.trim(),
                    referrer: document.referrer,
                    // captchaResponse: $(_o.elms.form).find('#captcha-response').val()
                },
                dataType: 'json',
                type: 'POST',
                success: function(result) {
                    if (result.success) {
                        $(_o.elms.loading).hide();
                        $(_o.elms.form_wrap).hide();
                        $(_o.elms.submitted).show();
                        $.scrollTo(0, 500);
                    } else {
                        _o.errors.push(_o.error_messages.captchaFailed);
                        _t.showErrors();
                        // grecaptcha.reset();
                    }
                },
                error: function() {
                    _o.errors.push(_o.error_messages.error);
                    _t.showErrors();
                    // grecaptcha.reset();
                }
            });
        },
        formValidation: function() {
            var _t = this,
                _o = _t.options,
                errors = 0;
            _o.errors = [];
            $(_o.elms.form).find('[data-required]').each(function() {
                if ($(this).val() === null || $(this).val().trim() === '') {
                    $(this).addClass('is-invalid');
                    errors++;
                } else {
                    $(this).removeClass('is-invalid');
                }
            });
            if (errors) {
                _o.errors.push(_o.error_messages.requiredFields);
            }
            return !errors;
        },
        resetErrors: function() {
            var _t = this,
                _o = _t.options;
            _o.errors = [];
            $(_o.elms.error).html('').hide();
        },
        showErrors: function() {
            var _t = this,
                _o = _t.options;
            $(_o.elms.loading).hide();
            $(_o.elms.submit).prop('disabled', false);
            var h = '';
            if (_o.errors.length) {
                $.each(_o.errors, function(i) {
                    h += _o.errors[i] + '<br>';
                });
                $(_o.elms.error).html(h).show();
            } else {
                $(_o.elms.error).html(h).hide();
            }
        }
    };

    $(document).ready(function() {
        common.init();
        // run contact_us script only on contact_us.html
        if ($(contact_us.options.elms.wrap).length) {
            contact_us.init();
        }
    });

})(jQuery);
