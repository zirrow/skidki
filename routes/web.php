<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/home', '/', 301);

Route::get('/', 'MainController@index')->name('main');
Route::post('/', 'MainController@filter')->name('main-filter');

Route::get('/contact-us', 'ContactUsController@index')->name('contact');
Route::post('/contact-us', 'ContactUsController@send')->name('contact-send');

Route::resource('/page', 'PageController');
