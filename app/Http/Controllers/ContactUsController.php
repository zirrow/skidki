<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
	public function index()
	{
		$data['page_title'] = 'Напишите нам';
		$data['page_description'] = 'Форма обратной связи';

		return view('contactUs', $data);
	}

	public function send(Request $request)
	{

		$this->validate($request, [
			'email' => 'required|email',
			'lastName' => 'required|max:25',
			'firstName' => 'required|max:25',
			'enquiryType' => 'required',
			'description' => 'required',

		]);

		Mail::to(env('ADMIN_MAIL'))->send(new ContactUsEmail($request));

		return response()->json(['success' => $request->all()], 200);
	}
}
