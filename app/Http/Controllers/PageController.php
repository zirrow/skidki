<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    	$data = [];
    	if($request->add_status){
		    $data['add_status'] = $request->add_status;
	    }

	    return view('page_create', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	    $image_link = '';
	    if($request->image){
		    $image_link = Storage::putFile('public/images', $request->image);
	    }

	    $data = [
	    	'title' => $request->title,
		    'description' => htmlentities($request->description),
		    'image' => $image_link,
		    'link' => $request->link,
		    'stop_date' => isset($request->stop_date) ? $request->stop_date : Carbon::now()->format('Y-m-d H:i:s')
	    ];

	    $status = false;

	    $page_id = Page::create($data);
	    if($page_id){
		    $status = $page_id;
	    }

	    return redirect()->action('PageController@index', ['add_status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$page = Page::find($id);

    	$now = Carbon::now()->format('Y-m-d H:i:s');

    	if($now < $page->stop_date){

		    $json = file_get_contents('http://getcitydetails.geobytes.com/GetCityDetails?fqcn='. $this->getIP());
		    $country_data = json_decode($json);

		    if($country_data->geobytesinternet == 'UA'){
			    return redirect()->to($page->link);
		    }
	    }

    	$data['page_data'] = [
    		'id' => $page->id,
		    'title' => $page->title,
		    'description' => htmlspecialchars_decode($page->description),
		    'image' => str_replace('public','storage', $page->image),
		    'link' => $page->link,
		    'stop_date' => $page->stop_date,
	    ];

	    $data['page_title'] = $page->title;
	    $data['page_description'] = strip_tags(htmlspecialchars_decode($page->description));

	    return view('page_show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $page = Page::find($id);

	    $data['page_data'] = [
		    'id' => $page->id,
		    'title' => $page->title,
		    'description' => htmlspecialchars_decode($page->description),
		    'image' => str_replace('public','storage', $page->image),
		    'link' => $page->link,
		    'stop_date' => $page->stop_date,
	    ];

	    $data['page_title'] = $page->title;
	    $data['page_description'] = strip_tags(htmlspecialchars_decode($page->description));

	    return view('page_create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        $page->title = $request->title;
        $page->description = htmlentities($request->description);
        $page->link = $request->link;
        $page->stop_date = $request->stop_date;
        $page->save();

	    return redirect()->action('MainController@index', ['update_status' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	Page::destroy($id);

	    return redirect()->action('MainController@index', ['destroy_status' => $id]);
    }

	private function getIP() {
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
			if (array_key_exists($key, $_SERVER) === true) {
				foreach (explode(',', $_SERVER[$key]) as $ip) {
					if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
						return $ip;
					}
				}
			}
		}
	}
}
