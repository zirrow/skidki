<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class BestController extends Controller
{
	public function index()
	{
		$pages = Page::limit(5)->orderBy('id', 'desc')->get();
		$data = [];

		foreach ($pages as $page){

			$desc = htmlspecialchars_decode($page->description);
			$desc = strip_tags((string) $desc);
			$desc = trim($desc);
			$desc = substr($desc, 0 , 150);

			$data[] = [
				'id' => $page->id,
				'title' => $page->title,
				'description' => $desc,
				'image' => str_replace('public','storage', $page->image),
				'link' => $page->link,
			];
		}

		return  $data;
	}
}
