<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request)
    {
    	$pages = Page::limit(30)->get();

	    $data['pages'] = [];

	    foreach ($pages as $page){

	    	$desc = htmlspecialchars_decode($page->description);
	    	$desc = strip_tags((string) $desc);
	    	$desc = trim($desc);
	    	$desc = substr($desc, 0 , 150);

		    $data['pages'][] = [
		    	'id' => $page->id,
		    	'title' => $page->title,
			    'description' => $desc,
			    'image' => str_replace('public','storage', $page->image),
			    'link' => $page->link,
		    ];
	    }

    	$data['page_title'] = 'Агригатор скидок и акций';
    	$data['page_description'] = 'Агрегатор скидок и акций всех топовых сайтов в интернете';

    	if($request->update_status){
    		$data['update_status'] = true;
	    }

	    if($request->destroy_status){
		    $data['destroy_status'] = true;
	    }

	    return view('main', $data);
    }

	public function filter(Request $request)
	{
		$limit = 30;
		$offset = $request->page * $limit;

		$pages = Page::limit($limit)->offset($offset)->get();

		$data = [];

		foreach ($pages as $page){

			$desc = htmlspecialchars_decode($page->description);
			$desc = strip_tags((string) $desc);
			$desc = trim($desc);
			$desc = substr($desc, 0 , 150);

			$data[] = [
				'id' => $page->id,
				'title' => $page->title,
				'description' => $desc,
				'image' => str_replace('public','storage', $page->image),
				'link' => $page->link,
			];
		}

		return response()->json($data, 200);
	}
}
